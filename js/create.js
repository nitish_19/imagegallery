const create = () => {
  //body element creation
  const body = document.getElementsByTagName('body')[0];
    
  // marquee element creation
  const marquee = document.createElement('marquee');
  const marqueeText = document.createTextNode('Eazydiner');
  marquee.append(marqueeText);
  
  // h1 element creation
  const h1 = document.createElement('h1');
  const h1Text = document.createTextNode('Explore high quality images');
  h1.append(h1Text);
    
    // Full Image Element creation
  const img = document.createElement('img');

    //styling full image using mouseover and mouseout events
  img.addEventListener('mouseover', () => {
    img.setAttribute('style','-webkit-filter: brightness(1.5); border-radius: 20px');
  },false);

  img.addEventListener('mouseout', () => {
    img.setAttribute('style','-webkit-filter: brightness(1.0); border-radius: 20px');
  },false);
    
  // button for zoom-in and zoom-out
  const button1 = document.createElement('button');
  const button2 = document.createElement('button');
  const divButton = document.createElement('div');

  const btnText1 = document.createTextNode('Zoom in');
  const btnText2 = document.createTextNode('Zoom out');
  button1.append(btnText1);
  button2.append(btnText2);
  divButton.appendChild(button1);
  divButton.appendChild(button2);

    // zoom-in button click event
  button1.onclick = () => {
    img.width = img.width+20;
  }
  //zoom-out button click event
  button2.onclick = () => {
    img.width = img.width-20;
  }

  // div for bar images
  const divBar = document.createElement('div');
    
  //Bar images creation and styling change on mouseover and mouseout events
  for(let i = 1; i <= 20; i++) {
    const newImage = document.createElement('img');

    newImage.addEventListener('mouseover', () => {
      newImage.setAttribute('style', '-webkit-filter: brightness(1.5)');
      newImage.setAttribute('style', 'border-radius: 20px');
      newImage.height = newImage.height+10;
      newImage.width = newImage.width+10;
    },false);

    newImage.addEventListener('mouseout', () => {
      newImage.setAttribute('style', '-webkit-filter: brightness(1.0)');
      newImage.setAttribute('style', 'border-radius: 20px');
      newImage.height = newImage.height-10;
      newImage.width = newImage.width-10;
    },false);

    //displaying the clicked image as full Image
    newImage.onclick = (e) => {
      img.src = e.target.src;
    }
    divBar.appendChild(newImage);
  } 
  body.appendChild(h1);
  body.appendChild(marquee);
  body.appendChild(img);
  body.appendChild(divButton);
  body.appendChild(divBar);
}