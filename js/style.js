const style = () => {
  // Getting html elements for style
  const body = document.getElementsByTagName('body')[0];
  const marquee = document.getElementsByTagName('marquee')[0];
  const h1 = document.getElementsByTagName('h1')[0];

  const fullImg = document.getElementsByTagName('img')[0];
  const button = document.getElementsByTagName('button');
  const barImg = document.getElementsByTagName('img');
    
  // bosy styles
  body.setAttribute('align', 'center');
  body.setAttribute('style', 'background-image: url(images/back.webp)');
    
  // marquee styles
  marquee.setAttribute('style', 'color:white; text-align: center');
  marquee.setAttribute('direction', 'up');

  // h1 styles
  h1.setAttribute('style','align: center; color: white; background: tomato; padding: 30px');
    
  // fullImg styles
  fullImg.setAttribute('src', 'images/pic6.jpg');
  fullImg.setAttribute('style', 'border-radius: 20px)');
    
  // zoom-in and zoom-out button styles
  for(let i = 0; i<button.length; i++){
    button[i].setAttribute('style', 'padding: 10px 20px; background-color: #f44336');
  }
    
  // Bar Images styles
  for(let i = 1; i<barImg.length; i++){
    barImg[i].setAttribute('src', 'images/pic' + i + '.jpg');
    barImg[i].setAttribute('width', '9%')
    barImg[i].setAttribute('display', 'block');
    barImg[i].setAttribute('style', 'border-radius: 20px');
    barImg[i].style.cursor = 'pointer';
    barImg[i].setAttribute('border', '2px solid rgb(255,255,255)');
  }
}